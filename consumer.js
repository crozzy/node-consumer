var bunyan = require('bunyan')
var format = require('string-format')
var request = require('request')
var replay = require('request-replay')
var xtend = require('xtend')

var RedisQueue = require('simple-redis-queue')

var config = require('./config')
 
// Create logger
var log = bunyan.createLogger({
  name: 'consumer',
  streams: [
    {
      level: 'info',
      path: config.logFile
    }
  ]
})

// Create a queue instance
module.exports = function(client, cb) {
  var queue = new RedisQueue(client);
  if (!queue) cb(new Error('Couldn\'t connect to queue'))
 
  // Listen for postbacks
  queue.on('message', function (queueName, payload) {
    try {
      var postBack = JSON.parse(payload)
      postBack.data.forEach(function(ele) {
        // Include the defaults from config
        var queryParams = xtend(config.urlParamDefaults, ele)
        makeRequest(format(postBack.endpoint.url, queryParams), postBack.endpoint.method)
      })
    }
    catch (e) {
      log.error({payload: payload}, 'Incorrect json document in queue')
    }
  });
 
  // Listen for errors
  queue.on('error', function (error) {
    console.log(error)
    logRequest(error)
  });

  // Start monitoring this queue
  queue.monitor(config.queueName)

  // Call cb with the queue once it is listening
  cb(null, queue)
}


// Helper functions

var makeRequest = function(url, method) {
  opts = {
    url: url,
    method: method
  }
  var start = new Date()
  replay(request(opts, function(err, resp, body) {
    if (err) {
      logRequest(err)
      return
    }
    var deliveryTime = new Date()
    var respData = {
      url: url,
      method: method,
      respStatus: resp.statusCode,
      respBody: body,
      deliveryTime: deliveryTime.toISOString(),
      respTime: deliveryTime - start
    }
    logRequest(null, respData)
  }), {
    retries: config.retryAttempts,
    factor: config.retryFactor
  })
  .on('replay', function(replay){
    logRequest(replay.error)
  })
}

var logRequest = function(err, data) {
  if (err) {
    log.error(err)
    return
  }
  log.info(data)
}
