# node-consumer #

This is a typical node micro-project

### Process entries from redis ###

Queuing is done with the redis list commands: LPUSH and BRPOP, future advancements could include the use of BRPOPLPUSH to push entries that are being worked on to an intermediate queue(list) to avoid data loss if the consumer goes down while processing.
 
Conceivably the consumer can be abstracted and used standalone
```
var consumer = require('./consumer')
var CLIENT = redis.createClient()

consumer(CLIENT, function(err, QUEUE){
// Now we are listening to the queue and consuming anything that was backed up
// QUEUE is a simple-redis-queue instance which in turn is a RedisQueue instance, you can also QUEUE.push()
})
```
### How do I get set up? ###

* Install node (debian based systems)
```
sudo apt-get install build-essential curl git nodejs npm -y
ln -s /usr/bin/node{js,}
```

* Install package in the node way.
```
git clone https://crozzy@bitbucket.org/crozzy/node-consumer.git && cd node-consumer
npm install
npm test
```

### Get it running ###

* Locally
```
node index.js
```

* Docker

If you don't have a redis container running:
```
docker pull redis
docker run --name kredis -v /path/to/host/dbstorage:/data -d redis redis-server --appendonly yes
```
then:
```
docker build -t node-consumer:latest -rm node-consumer/
docker run --link kredis:db -i -v /path/to/host/logs:/var/log/node-consumer -t node-consumer:latest
```

### Configuration ###

Configuration is handled with the node package rc. config.js can be extended with env vars (the unix way). Or by creating an .nodeconsumerc file with JSON object.

* nodeconsume_logFile: The file we will be logging to.
* nodeconsume_queueName: The name of the queue to consume from (redis key).
* nodeconsume_redisHost: The redis host default 127.0.0.1
* nodeconsume_redisPost: The redis port default 6379
* nodeconsume_urlParamDefaults: A map (or object literal) with default url param keys.
* nodeconsume_retryAttempts: How many retry attempts to make when getting a request error.
* nodeconsume_retryFactor: The exponential factor to use.

### Logging ###

The node logging is implemented with bunyan (https://github.com/trentm/node-bunyan).
The concept is to log everything in JSON to make log processing easier and more pluggable, with the amount of data we are logging having it easily parsed by another application is neccessary. And with the bunyan binary it becomes human readable.