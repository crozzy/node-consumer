module.exports = require('rc')('nodeconsume', {

  // Base application config
  urlParamDefaults : {
    foo : 'default',
    bar : 'mas'
  },
  logFile: '/tmp/consumer.log',
  queueName: 'postback2',
  redisHost: '127.0.0.1',
  redisPort: 6379,
  retryAttempts: 0,
  retryFactor: 3
})

