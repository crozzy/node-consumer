FROM ubuntu:14.04

RUN apt-get update && \
    apt-get install nodejs npm git -y

RUN ln -s /usr/bin/nodejs /usr/bin/node

ADD . /opt/node-consumer

RUN rm -rf /opt/node-consumer/node_module && \
    cd /opt/node-consumer; npm install

ENV nodeconsume_logFile /var/log/node-consumer/consumer.log

CMD ["/bin/bash", "/opt/node-consumer/upstart.sh"]
