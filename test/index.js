var fs = require('fs')
var lazy = require('lazy')
var redis = require('redis')
var rimraf = require('rimraf')
var test = require('tape')

var consumer = require('../consumer')
var config = require('../config')

var CLIENT = redis.createClient(config.redisPort, config.RedisHost)
consumer(CLIENT, verify)

function verify(err, QUEUE) {
  if (err) return console.error(err)

  test('Good data', function(t) {
    var testObj = {
      endpoint: {
        url: "http://httpbin.org/get?this={foo}",
        method: "GET"
      },
      data: [
        {
          foo: "algo",
          bar: "tester"
        },
        {
          boo: "algo",
          ddd: "mas"
        }
      ],
    }
    var client = redis.createClient()
    var result = client.lpush(config.queueName, JSON.stringify(testObj))
    t.end()
  })

  test('Check log good', function(t) {
    t.plan(3)
    var changeCounter = 0

    fs.watch(config.logFile, {persistent: false}, function(event, filename) {
      t.equal(event, 'change')
      changeCounter +=1
      if (changeCounter == 2) {
        var lazyLogStream = lazy(fs.createReadStream(config.logFile))
        lazyLogStream.lines.take(2)
        .join(function(line) {
          var something = JSON.parse('[' + line.toString() + ']')
          var urls = []
          something.forEach(function(line) {
            urls.push(line['url'])
          })
          t.deepEqual(
            urls.sort(), 
            ['http://httpbin.org/get?this=algo', 'http://httpbin.org/get?this=default'])
        })
      }
    })
    fs.unwatchFile(config.logFile)
  })

  test('Bad data', function(t) {
    var testObj = {
      something: "what am i doing here?"
    }
    var testString = 'ErrorErrorError'
    var client = redis.createClient()
    client.lpush(config.queueName, JSON.stringify(testObj))
    client.lpush(config.queueName, testString)
    t.end()
  })

  test('Check log bad', function(t) {

    fs.watch(config.logFile, {persistent: false}, function(event, filename) {
      t.equal(event, 'change')
      var lazyLogStream = lazy(fs.createReadStream(config.logFile))
      lazyLogStream.lines.skip(2).take(2)
      .join(function(line) {
        var something = JSON.parse('[' + line.toString() + ']')
        var payload = []
        var level  = []
        something.forEach(function(line) {
          payload.push(line['payload'])
          level.push(line['level'])
        })
        t.deepEqual(
          payload.sort(), 
          ['ErrorErrorError', '{\"something\":\"what am i doing here?\"}'])
        t.ok(level.every(function(elem){return elem == 50}))
      })
    })
    fs.unwatchFile(config.logFile)
    t.end()
  })

  test('Teardown data', function(t) {
    rimraf(config.logFile, function(err) {
      t.error(err, 'no error')
      t.end()
      process.exit()
    })
  })
}
