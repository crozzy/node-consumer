var redis = require('redis')

var config = require('./config')
var consumer = require('./consumer')

var client = redis.createClient(config.redisPort, config.redisHost)
consumer(client, function(err, queue) {
  if (!err) console.log('Monitoring queue')
})
